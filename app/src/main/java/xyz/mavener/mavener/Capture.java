package xyz.mavener.mavener;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;

public class Capture extends AppCompatActivity {

    // Initialize static variables
    private static final String TAG = "Capture";
    private static final int LAUNCH_CAMERA = 0;
    protected static final String DATA_PATH =
            Environment.getExternalStorageDirectory().toString() + "/Mavener/";

    // Declare app variables
    protected static Bitmap BITMAP;
    protected static Uri URI;
    protected static String IMAGE_PATH;
    protected boolean taken;

    // Declare widget variables
    protected Button button;
    protected ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.id.activity_image);
        Log.i(TAG, "onCreate");

        // Initialize app variables
        taken = false;
        IMAGE_PATH = DATA_PATH + "Images/imagejpg";

        // Initialize widget variables
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v(TAG, "Button clicked.");
                launchCamera();
            }
        });
        imageView = (ImageView) findViewById(R.id.image);
    }

    protected void launchCamera() {
        File imageFile = new File(IMAGE_PATH);
        URI = Uri.fromFile(imageFile);
        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, URI);

        Log.v(TAG, "Launching camera...");
        startActivityForResult(intent, LAUNCH_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LAUNCH_CAMERA && resultCode == RESULT_OK) {
            Log.v(TAG, "Camera activity successful.");
            taken = true;
            BITMAP = (Bitmap) data.getExtras().get("data");

            imageView.setImageBitmap(BITMAP);
        } else {
            Log.v(TAG, "Camera activity not successful.");
        }
    }

    public static Bitmap getBitmap() {
        return BITMAP;
    }

    public static Uri getUri() {
        return URI;
    }

    public static String getImagePath() {
        return IMAGE_PATH;
    }

}