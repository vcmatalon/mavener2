package xyz.mavener.mavener;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.googlecode.tesseract.android.TessBaseAPI;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;

public class OCR extends AppCompatActivity {
    // Initialize static variables
    private static final String TAG = "OCR";
    public static final String DATA_PATH = Environment.getDataDirectory().toString() + "/Mavener/";
    public static final String LANGUAGE = "eng";

    // Get Capture.java variables
    protected Bitmap bitmap = Capture.getBitmap();
    protected Uri uri = Capture.getUri();
    protected String imagePath = Capture.getImagePath();

    // Declare app variables
    protected static String RECOGNIZED_TEXT;
    protected TessBaseAPI baseAPI;
    protected Bitmap configuredBitmap;
    protected boolean configured;
    protected boolean initialized;
    protected boolean completed;


    // Declare widget variables
 //<<< PENDING >>>
    protected ProgressBar _progressbar;
    protected ImageView imageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ocr);

        // Initialize app variables
        configured = false;
        initialized = false;
        completed = false;

        // Initialize widget variables
        // <<< PENDING >>>
        imageView.setImageBitmap(bitmap);
        //progress bar

        // Create directories needed by Tess-Two if need be
        File appDirectory = new File(DATA_PATH);
        File tessdataDirectory = new File(DATA_PATH, "tessdata");
        createDirectories(appDirectory, tessdataDirectory);

        // Extract language file if need be
        String languageFileName = LANGUAGE + ".traineddata";
        File destination = new File(tessdataDirectory, languageFileName);
        extractLanguageFiles(languageFileName, destination);

        // Perform OCR
        configuredBitmap = configureBitmap(uri, imagePath);
        baseAPI = initializeAPI();
        performOCR(configuredBitmap, baseAPI);
    }

    protected Bitmap configureBitmap(Uri uri, String path) {
        InputStream inputStream = null;
        try {
            Log.v(TAG, "Configuring bitmap...");
            // Create and configure
            inputStream = getContentResolver().openInputStream(uri);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bm = BitmapFactory.decodeStream(inputStream, null, options);

            // Read EXIF
            ExifInterface exif = null;
            try {
                exif = new ExifInterface(path);
            } catch (IOException e) {
                Log.e(TAG, "ERROR: Couldn't read EXIF data to rotate");
                e.printStackTrace();
            }
            if (exif != null) {

                // Find rotation angle: theta
                int orientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int theta = 0;
                if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                    theta = 90;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                    theta = 180;
                } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                    theta = 270;
                }

                // Rotate bitmap
                Matrix matrix = new Matrix();
                matrix.setRotate(theta, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
                Bitmap C_BITMAP = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);

                Log.v(TAG, "Bitmap configuration successful.");
                configured = true;

                return C_BITMAP;
            }
        } catch (FileNotFoundException e) {
            Log.e(TAG, "ERROR: Uri doesn't reference image");
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e(TAG, "ERROR: Can't close InputStream used to open Uri");
                    e.printStackTrace();
                }
            }
        }
    }

    private void createDirectories(File appDirectory, File tessdataDirectory) {
        if (!appDirectory.isDirectory()) { // Make both app and tessdata directories
            if (tessdataDirectory.mkdirs()) {
                Log.i(TAG, "Created app and tessdata directories.");
            } else {
                Log.e(TAG, "ERROR: Creation of app and tessdata directories failed.");
            }
        } else if (!tessdataDirectory.isDirectory()) { // Make tessdata directory
            if (tessdataDirectory.mkdir()) {
                Log.i(TAG, "Created tessdata directory.");
            } else {
                Log.e(TAG, "ERROR: Creation of tessdata directory failed.");
            }
        }
    }

    private void extractLanguageFiles(String languageFileName, File destination) {
        if (!destination.exists()) {
            try {
                AssetManager assetManager = getAssets();
                InputStream source = assetManager.open("tessdata/" + languageFileName);
                FileUtils.copyInputStreamToFile(source, destination);
                source.close();
                assetManager.close();
                Log.i(TAG, "Extracted language data into tessdata.");
            } catch (IOException e) {
                Log.e(TAG, "ERROR: Unable to extract language data into tessdata.", e);
            }
        }
    }

    private TessBaseAPI initializeAPI() {
        TessBaseAPI baseAPI = new TessBaseAPI();
        if (baseAPI.init(DATA_PATH, LANGUAGE)) {
            initialized = true;
            Log.i(TAG, "Initialized of baseApi.");
            return baseAPI;
        } else {
            Log.e(TAG, "ERROR: Initialization of TessBaseApi failed.");
            return null;
        }
    }

    protected void performOCR(Bitmap bitmap, TessBaseAPI baseAPI) {
        baseAPI.setImage(bitmap);
        RECOGNIZED_TEXT = baseAPI.getUTF8Text();
        completed = true;
        Log.i(TAG, "Completed OCR.");

        bitmap.recycle();
        baseAPI.end();
    }

 // <<< PENDING >>>
    // Use booleans to save on work already done
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG, "onSaveInstanceState");
    }

    // Use booleans to save on work already done
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(TAG, "onRestoreInstanceState");
    }
}
